import toast from "@/src/modules/ui/Toast";
import { useCallback } from "react";

export default function toastify() {
  const notify = useCallback((type, message) => {
    toast({ type, message });
  }, []);
  const dismiss = useCallback(() => {
    toast.dismiss();
  }, []);
  return { notify, dismiss };
}
