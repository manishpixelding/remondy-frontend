import axios from "axios";

axios.defaults.baseURL = "https://remondy-backend.herokuapp.com/api/";
// axios.defaults.baseURL = "http://localhost:8000/api/";
axios.defaults.withCredentials = true;
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.headers = {
  Accept: "application/json",
  "Content-Type": "application/json",
};
;

let refresh = false;



axios.interceptors.response.use(
  (resp) => resp,

  async (error) => {
    if (error.response.status === 401 && !refresh) {
      const res = await axios.post("token/refresh/", { withCredentials: true });
      refresh = true;
      return error;
    }

    if (error.response.status === 200) {
      refresh = true;
      return axios(error.config);
    }
  }
);
