import Loading from "@/src/modules/ui/loading";
import { DataGrid } from "@mui/x-data-grid";
import * as React from "react";
import user_columns from "../../static/user_columns";

export default function UserList({ users, isLoading }) {
  return (
    <div style={{ height: "90vh", width: "100%" }}>
      {isLoading ? (
        <Loading />
      ) : (
        <DataGrid
          rows={users || []}
          columns={user_columns}
          pageSize={10}
          rowsPerPageOptions={[2]}
          checkboxSelection
        />
      )}
    </div>
  );
}
