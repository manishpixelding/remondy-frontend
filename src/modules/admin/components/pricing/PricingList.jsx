import Loading from "@/src/modules/ui/loading";
import { DataGrid } from "@mui/x-data-grid";
import * as React from "react";
import pricing_columns from "../../static/pricing_columns";

export default function PricingList({ pricings, isLoading }) {
  return (
    <div style={{ height: "90vh", width: "100%" }}>
      {isLoading ? (
        <Loading />
      ) : (
        <DataGrid
          rows={pricings || []}
          columns={pricing_columns}
          pageSize={10}
          rowsPerPageOptions={[2]}
          checkboxSelection
        />
      )}
    </div>
  );
}
