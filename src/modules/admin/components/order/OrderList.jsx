import Loading from "@/src/modules/ui/loading";
import { DataGrid } from "@mui/x-data-grid";
import * as React from "react";
import order_columns from "../../static/order_columns";

export default function OrderList({ all_orders, isLoading }) {
  return (
    <div style={{ height: "90vh", width: "100%" }}>
      {isLoading ? (
        <Loading />
      ) : (
        <DataGrid
          rows={all_orders || []}
          columns={order_columns}
          pageSize={10}
          rowsPerPageOptions={[2]}
          checkboxSelection
        />
      )}
    </div>
  );
}
