const user_columns = [
  { field: "id", headerName: "ID", width: 70 },
  { field: "email", headerName: "Email", width: 130 },
  { field: "first_name", headerName: "First Name", width: 130 },
  { field: "last_name", headerName: "Last Name", width: 130 },
  {
    field: "fullname",
    headerName: "Fullname",
    width: 130,
    valueGetter: (params) =>
      `${params.row.first_name + " " + params.row.last_name}`,
  },
  {
    field: "is_maintainer",
    type: "bool",
    headerName: "Maintainer",
    width: 130,
  },
  { field: "is_customer", headerName: "Customer", width: 130 },
];

export default user_columns;
