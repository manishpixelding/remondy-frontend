const pricing_columns = [
  { field: "id", headerName: "ID", width: 70 },
  { field: "title", headerName: "Title", width: 160 },
  {
    field: "base_price",
    headerName: "Base Price",
    type: "number",
    width: 120,
  },
];

export default pricing_columns;
