import { getPaymentLetter } from "@/src/utils/script_chunks";

const order_columns = [
  { field: "id", headerName: "ID", width: 70 },
  { field: "customer_name", headerName: "Customer", width: 130 },
  { field: "address", headerName: "Address", width: 130 },
  { field: "device", headerName: "Device", width: 130 },
  { field: "damage_desc", headerName: "Damage Description", width: 130 },
  { field: "shipper", headerName: "Shipper", width: 130 },
  { field: "depot", headerName: "Depot", width: 130 },
  {
    field: "ammount",
    headerName: "Ammount",
    type: "number",
    width: 90,
  },
  {
    field: "payment_status",
    headerName: "Payment",
    type: "number",
    width: 90,
    valueGetter: (params) => getPaymentLetter(params.row.payment_status),
  },
];

export default order_columns;
