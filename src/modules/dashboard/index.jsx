import { setAuth } from "@/src/redux/slices/auth/authSlice";
import { setCurrentUser } from "@/src/redux/slices/auth/userSlice";
import {
  setShowBrandForm,
  setShowDamageForm,
  setShowOrderForm,
  setShowPricingForm,
  setShowZipcodeForm,
} from "@/src/redux/slices/order/orderFormSlice";
import { getPaymentLetter } from "@/src/utils/script_chunks";
import toastify from "@/src/utils/toastify";
import { DataGrid } from "@mui/x-data-grid";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useQuery } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import MainLayout from "../ui/layouts/MainLayout";
import Loading from "../ui/loading";
import BrandForm from "./components/BrandForm";
import DamageForm from "./components/DamageForm";
import OrderForm from "./components/OrderForm";
import PriceForm from "./components/PriceForm";
import ZipCodeForm from "./components/ZipCodeForm";

export default function Dashboard() {
  const router = useRouter();
  const { notify } = toastify();

  const isAuthenticated = useSelector((state) => state.auth.value);

  const { isLoading, error, data } = useQuery("currentUserOrders", () =>
    axios.get("order/user").then((res) => res.data)
  );

  const showZipcodeForm = useSelector(
    (state) => state.orderForm.showZipcodeForm
  );

  const showOrderForm = useSelector((state) => state.orderForm.showOrderForm);

  const showDamageForm = useSelector((state) => state.orderForm.showDamageForm);

  const showBrandForm = useSelector((state) => state.orderForm.showBrandForm);

  const showPricingForm = useSelector(
    (state) => state.orderForm.showPricingForm
  );
  const currentUser = useSelector((state) => state.user.currentUser);

  const dispatch = useDispatch();


  useEffect(() => {
    (async () => {
      if (!isAuthenticated) {
        const res = await axios.get("account/user", { withCredentials: true });

        if (res !== undefined && res.status === 200) {
          dispatch(setAuth(true));
          dispatch(setCurrentUser(res.data));
        } else {
          notify("error", "Please Login to continue");
          dispatch(setAuth(false));
          router.push("/auth/login");
          // TODO : ROUTE TO LOGIN
        }
      } else {
        router.push("/auth/login");
      }
    })();
  }, [isAuthenticated]);
  


 
  const columns = [
    { field: "id", headerName: "ID", width: 70 },
    { field: "customer_name", headerName: "Customer", width: 130 },
    { field: "address", headerName: "Address", width: 130 },
    { field: "device", headerName: "Device", width: 130 },
    { field: "damage_desc", headerName: "Damage Description", width: 130 },
    { field: "shipper", headerName: "Shipper", width: 130 },
    { field: "depot", headerName: "Depot", width: 130 },
    {
      field: "ammount",
      headerName: "Ammount",
      type: "number",
      width: 90,
    },
    {
      field: "payment_status",
      headerName: "Payment",
      type: "number",
      width: 90,
      valueGetter: (params) => getPaymentLetter(params.row.payment_status),
    },
  ];

 
  
  return (
    <MainLayout>
      <main>
        <section className="grid grid-cols-1 md:grid-cols-3">
          <div className="col-span-2 md:pt-5">
            <article>
              <span className="flex items-center">
                <img
                  src="http://via.placeholder.com/200x200"
                  className="w-10 rounded-full shadow-md"
                />
                <span className="flex-row mx-2 font-medium leading-tight text-left capitalize text-blue">
                  <h1 className="">
                    {currentUser.first_name + " " + currentUser.last_name}
                  </h1>

                  <h1 className="text-xs font-medium lowercase text-red">
                    {currentUser.email}
                  </h1>
                </span>
              </span>
            </article>
          </div>

          <div className="flex items-end justify-end w-full ">
            <section className="flex items-end justify-end ">
              <button
                className="btn-primary"
                onClick={(e) => dispatch(setShowZipcodeForm(!showZipcodeForm))}>
                Place an order
              </button>

              <ZipCodeForm
                isOpen={showZipcodeForm}
                setIsOpen={setShowZipcodeForm}
              />
              <OrderForm isOpen={showOrderForm} setIsOpen={setShowOrderForm} />
              <DamageForm
                isOpen={showDamageForm}
                setIsOpen={setShowDamageForm}
              />
              <PriceForm
                isOpen={showPricingForm}
                setIsOpen={setShowPricingForm}
              />
              <BrandForm isOpen={showBrandForm} setIsOpen={setShowBrandForm} />
            </section>
          </div>
        </section>
        <section
          className="w-full h-full my-5"
          style={{ height: 400, width: "100%" }}>
          {isLoading ? (
            <Loading />
          ) : (
            <DataGrid
              rows={data || []}
              columns={columns}
              pageSize={5}
              rowsPerPageOptions={[5]}
              checkboxSelection
            />
          )}
        </section>
      </main>
    </MainLayout>
  );
}
