import FormModal from "@/modules/ui/modal/form-modal";
import {
  setShowBrandForm,
  setShowZipcodeForm,
} from "@/src/redux/slices/order/orderFormSlice";
import { setOrder } from "@/src/redux/slices/order/orderSlice";
import toastify from "@/src/utils/toastify";
import { ZipcodeValidationSchema } from "@/src/validators/order/orderValidator";
import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";

const zipcodeOptions = [
  { value: "10551", label: "10551" },
  { value: "10553", label: "10553" },
  { value: "10555", label: "10555" },
  { value: "10585", label: "10585" },
  { value: "10587", label: "10587" },
  { value: "10589", label: "10589" },
  { value: "10623", label: "10623" },
  { value: "10625", label: "10625" },
  { value: "10627", label: "10627" },
  { value: "10629", label: "10629" },
  { value: "13627", label: "13627" },
  { value: "13629", label: "13629" },
  { value: "14050", label: "14050" },
  { value: "14057", label: "14057" },
  { value: "14059", label: "14059" },
];

export default function ZipCodeForm({ isOpen, setIsOpen }) {
  const { notify } = toastify();
  const dispatch = useDispatch();
  const order = useSelector((state) => state.order.order);
  const formOptions = { resolver: yupResolver(ZipcodeValidationSchema) };

  const { handleSubmit, formState, control } = useForm(formOptions);

  const onSubmit = (data) => {
    if (formState.errors) {
      console.log("errors", data, formState.touchedFields, formState.errors);
    }
    dispatch(setOrder({ ...order, ...data }));
    dispatch(setShowZipcodeForm(false));
    dispatch(setShowBrandForm(true));
  };

  return (
    <FormModal
      setIsOpen={setIsOpen}
      isOpen={isOpen}
      contentLabel={"Zipcode Selection Form"}
      nextButtonLabel="Continue next">
      <form className="w-full h-full form" onSubmit={handleSubmit(onSubmit)}>
        <div className="h-[60vh] grid  ">
          <section>
            <div className="w-full h-full text-zinc-700">
              <article className="w-full ">
                <h1 className="text-xl">Lets Place an Order</h1>
                <p className="text-xs text-zinc-500">
                  Hey there, Welcome to Remondy System we are currently
                  aviliable only in some area's, please select down your postal
                  code to proceed an order.
                </p>
              </article>

              <section className="flex-row justify-between h-full">
                <div className="py-6">
                  <span className="text-sm"> Select Your ZipCode</span>
                  <Controller
                    control={control}
                    defaultValue={zipcodeOptions[0]}
                    name="zipcode"
                    render={({ field: { onChange, onBlur, value, ref } }) => (
                      <Select
                        inputRef={ref}
                        classNamePrefix="bg-blue"
                        options={zipcodeOptions}
                        value={zipcodeOptions.find((c) => c.value === value)}
                        onChange={(val) => onChange(val.value)}
                      />
                    )}
                  />
                </div>
              </section>
            </div>
          </section>
          <section className="flex items-end justify-end ">
            <input
              type="submit"
              className="mt-auto btn-primary"
              value="Verify ZIPCode"
            />
          </section>
        </div>
      </form>
    </FormModal>
  );
}
