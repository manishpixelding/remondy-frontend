import FormModal from "@/modules/ui/modal/form-modal";
import {
  setShowBrandForm,
  setShowDamageForm,
} from "@/src/redux/slices/order/orderFormSlice";
import { setOrder } from "@/src/redux/slices/order/orderSlice";
import toastify from "@/src/utils/toastify";
import { BrandValidationSchema } from "@/src/validators/order/orderValidator";
import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";

const brandSelectionOptions = [
  { value: "samsung", label: "samsung" },
  { value: "apple", label: "apple" },
  { value: "motorolla", label: "motorolla" },
  { value: "xiaomi", label: "xiaomi" },
];

export default function BrandForm({ isOpen, setIsOpen }) {
  const dispatch = useDispatch();
  const { notify } = toastify();

  const formOptions = { resolver: yupResolver(BrandValidationSchema) };
  const order = useSelector((state) => state.order.order);

  const { handleSubmit, formState, control } = useForm(formOptions);
  const { errors } = formState;

  const handleBrandSubmit = (data) => {
    if (errors) {
      Object.values(errors).map((value) => notify("error", value));
    }
    dispatch(setOrder({ ...order, ...data }));
    dispatch(setShowBrandForm(false));
    dispatch(setShowDamageForm(true));
  };

  return (
    <FormModal
      setIsOpen={setIsOpen}
      isOpen={isOpen}
      contentLabel={"Brand Selection Form"}
      nextButtonLabel="Continue next">
      <form
        onSubmit={handleSubmit(handleBrandSubmit)}
        className="w-full h-full form">
        <div className="h-[60vh] grid  ">
          <section>
            <article className="w-full ">
              <h1 className="text-xl">Lets Place an Order</h1>
              <p className="text-xs text-zinc-500">
                Hey there, Welcome to Remondy System we are please select your
                brand, so that we can move more into it.
              </p>
            </article>

            <section className="h-full ">
              <div className="py-6">
                <span className="text-sm">Select Your Brand</span>

                <Controller
                  control={control}
                  defaultValue={brandSelectionOptions[0]}
                  name="device"
                  as={Select}
                  render={({ field: { onChange, onBlur, value, ref } }) => (
                    <Select
                      inputRef={ref}
                      classNamePrefix="bg-blue"
                      options={brandSelectionOptions}
                      value={brandSelectionOptions.find(
                        (c) => c.value === value
                      )}
                      onChange={(val) => onChange(val.value)}
                    />
                  )}
                />
              </div>
            </section>
          </section>
          <section className="flex items-end justify-end">
            <input className="btn-primary" type="submit" value="Select Brand" />
          </section>
        </div>
      </form>
    </FormModal>
  );
}
