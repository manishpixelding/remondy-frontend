import FormModal from "@/modules/ui/modal/form-modal";
import {
  setShowOrderForm,
  setShowPricingForm,
} from "@/src/redux/slices/order/orderFormSlice";
import toastify from "@/src/utils/toastify";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import axios from "axios";
import React, { useState } from "react";
import { useQuery } from "react-query";
import { useDispatch } from "react-redux";

export default function PriceForm({ isOpen, setIsOpen }) {
  const dispatch = useDispatch();
  const { notify } = toastify();
  const [selectedPricing, setSelectedPricing] = useState(0);

  const fetchPricing = () =>
    axios.get("/pricing").then((res) => {
      let base_price = res.data[0].base_price;

      res.data.find((element, index) => {
        if (element.base_price > base_price) {
          base_price = element.base_price;
          setSelectedPricing(index);
        }
      });

      return res.data;
    });

  const {
    isLoading,
    error,
    data: pricings,
    isFetching,
  } = useQuery("pricings", fetchPricing);

  const handlePriceSubmit = (data) => {
    if (error) {
      notify("error", error);
    }
    dispatch(setShowPricingForm(false));
    dispatch(setShowOrderForm(true));
  };

  return (
    <FormModal
      setIsOpen={setIsOpen}
      isOpen={isOpen}
      contentLabel={"Brand Selection Form"}
      nextButtonLabel="Continue next">
      <form className="w-full h-full form">
        <div className="h-[60vh] grid  ">
          <section>
            <article className="w-full ">
              <h1 className="text-xl">Lets Place an Order</h1>
              <p className="text-xs text-zinc-500">
                Hey there, Welcome to Remondy System we have the following price
                models.
              </p>
            </article>

            <section className="h-full ">
              <div className="py-6">
                <span className="text-sm">Select Price Model</span>

                <section className="grid w-full grid-cols-1 gap-5 px-2 my-5 cursor-pointer md:grid-cols-3">
                  {pricings?.map((pricing, index) => (
                    <Card
                      onClick={(e) => setSelectedPricing(index)}
                      key={index}
                      className={`transform origin-center transition ease-in-out duration-200 text-center w-full ${
                        index === selectedPricing
                          ? "border border-red border-3  scale-[110%]  "
                          : "scale-[90%]"
                      }`}
                      elevation={3}
                      onSelect={(e) => console.log(e)}>
                      <CardContent className="text-center ">
                        <section>
                          <Typography
                            variant="h5"
                            component="div"
                            className={`flex items-center justify-center py-14 m-auto font-bold text-white capitalize ${
                              index === selectedPricing
                                ? "bg-red rounded"
                                : "bg-blue rounded-sm"
                            }`}>
                            {pricing.title}
                          </Typography>
                        </section>

                        <Typography
                          variant="h7"
                          component="div"
                          className="py-4 uppercase text-blue ">
                          ${pricing.base_price}
                        </Typography>
                      </CardContent>
                    </Card>
                  ))}
                </section>
              </div>
            </section>
          </section>
          <section className="flex items-end justify-end">
            <Button
              type={"submit"}
              size={"large"}
              color={"secondary"}
              variant={"outlined"}
              onClick={handlePriceSubmit}>
              Submit Pricing
            </Button>
          </section>
        </div>
      </form>
    </FormModal>
  );
}
