import FormModal from "@/modules/ui/modal/form-modal";
import { setShowOrderForm } from "@/src/redux/slices/order/orderFormSlice";
import toastify from "@/src/utils/toastify";
import { OrderValidationSchema } from "@/src/validators/order/orderValidator";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import Loading from "../../ui/loading";

function OrderForm({ isOpen, setIsOpen }) {
  const dispatch = useDispatch();
  const { notify } = toastify();
  const router = useRouter();
  const order = useSelector((state) => state.order.order);

  const { mutateAsync, error, isError } = useMutation((newOrder) => {
    return axios.post("/order/create", { ...newOrder });
  });

  const formOptions = { resolver: yupResolver(OrderValidationSchema) };

  const { register, handleSubmit, formState } = useForm(formOptions);

  const { errors, isValid, isSubmitted, isSubmitting, isSubmitSuccessful } =
    formState;

  const handleOrderSubmit = async (data, e) => {
    mutateAsync({ ...order, ...data });
    notify("success", "Order Placed");
    dispatch(setShowOrderForm(false));
  };

  useEffect(() => {
    console.log(errors, isValid, isSubmitted, isSubmitting, isSubmitSuccessful);
  }, [errors, isValid, isSubmitted, isSubmitting, isSubmitSuccessful]);
  return (
    <FormModal
      setIsOpen={setIsOpen}
      isOpen={isOpen}
      contentLabel={"Zipcode Selection Form"}
      nextButtonLabel={"Continue next"}>
      {isSubmitting ? (
        <Loading />
      ) : (
        <form
          className="w-full grid h-[60vh]"
          onSubmit={handleSubmit(handleOrderSubmit)}
          method="POST">
          <section>
            <div className="w-full h-full text-zinc-700">
              <article className="w-full ">
                <h1 className="text-xl">Lets Place an Order</h1>
                <p className="text-xs text-zinc-500">
                  Hey there,let make your device work and rock again, please
                  fill down all the information correctly to proceed an order.
                </p>
              </article>

              <section className="grid h-full grid-flow-row gap-0 mt-2 ">
                <div className="">
                  <span className="inline-block text-sm">
                    Please enter your fullname?
                  </span>
                  <input
                    {...register("customer_name")}
                    type="text"
                    required
                    placeholder="Enter your fullname"
                    className="inline-block w-full"
                  />
                </div>

                <div className="">
                  <span className="inline-block text-sm text-zinc-500">
                    Please giveus your address?
                  </span>
                  <input
                    {...register("address")}
                    type="text"
                    required
                    placeholder="Enter your address"
                    className="inline-block w-full"
                  />
                </div>

                <div className="grid w-full grid-cols-2 gap-x-2">
                  <div className="">
                    <span className="inline-block text-sm text-zinc-500">
                      Please enter your shipper?
                    </span>
                    <input
                      {...register("shipper")}
                      type="text"
                      required
                      placeholder="enter your shipper info."
                      className="inline-block w-full"
                    />
                  </div>

                  <div className="">
                    <span className="inline-block text-sm text-zinc-500">
                      Please enter your depot?
                    </span>
                    <input
                      {...register("depot")}
                      type="text"
                      required
                      placeholder="Enter your depot."
                      className="inline-block w-full"
                    />
                  </div>
                </div>

                <div className="">
                  <span className="inline-block text-sm text-zinc-500">
                    Please enter the ammount?
                  </span>
                  <input
                    {...register("ammount")}
                    type="number"
                    required
                    className="inline-block w-full"
                  />
                </div>
              </section>
            </div>
          </section>

          <section className="flex items-end justify-end">
            <input type="submit" className="btn-primary" value="Place Order" />
          </section>
        </form>
      )}
    </FormModal>
  );
}

export default OrderForm;
