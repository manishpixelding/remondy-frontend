import FormModal from "@/modules/ui/modal/form-modal";
import {
  setShowDamageForm,
  setShowPricingForm,
} from "@/src/redux/slices/order/orderFormSlice";
import { setOrder } from "@/src/redux/slices/order/orderSlice";
import toastify from "@/src/utils/toastify";
import { DamageValidationSchema } from "@/src/validators/order/orderValidator";
import { yupResolver } from "@hookform/resolvers/yup";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";

export default function DamageForm({ isOpen, setIsOpen }) {
  const { notify } = toastify();
  const dispatch = useDispatch();

  const order = useSelector((state) => state.order.order);

  const formOptions = { resolver: yupResolver(DamageValidationSchema) };

  const { register, handleSubmit, formState, errors } = useForm(formOptions);

  const { isValid, isSubmitted } = formState;

  const handleDamageSubmit = (data) => {
    if (isSubmitted && !isValid) {
      notify("error", "Something went wrong, Please enter correct value.");
    } else {
      console.log(data.damage_image, errors);

      const body = {
        damage_image: data.damage_image,
        damage_desc: data.damage_desc,
      };

      dispatch(
        setOrder({
          ...order,
          ...body,
        })
      );
      dispatch(setShowDamageForm(false));
      dispatch(setShowPricingForm(true));
    }
  };
  useEffect(() => {
    console.log(errors);
  }, [errors]);

  return (
    <FormModal
      setIsOpen={setIsOpen}
      isOpen={isOpen}
      contentLabel={"Zipcode Selection Form"}
      nextButtonLabel="Continue next">
      <form
        onSubmit={handleSubmit(handleDamageSubmit)}
        className="w-full h-[60vh] form"
        encType="multipart/form-data">
        <section>
          <div className="w-full h-full text-zinc-700">
            <article className="w-full ">
              <h1 className="text-xl">Please Explain your Damage</h1>
              <p className="text-xs text-zinc-500">
                Hey there, Welcome to Remondy System we are currently aviliable
                only in some area's, please explain your damage so that we can
                work best for you.
              </p>
            </article>

            <section className="grid h-full grid-flow-row gap-3 my-5">
              <div className="">
                <span className="inline-block text-sm">
                  Please upload your images?
                </span>
                <input
                  type="file"
                  required
                  className="inline-block w-full"
                  {...register("damage_image")}
                />
              </div>
              <div className="">
                <span className="inline-block text-sm">
                  Please enter your damage description?
                </span>
                <textarea
                  cols="20"
                  rows={10}
                  {...register("damage_desc")}
                  placeholder="Please explain your damage so that we can get more ideas into you machine problems."
                  className="inline-block w-full rounded-sm shadow-sm"
                />
              </div>
            </section>
          </div>
        </section>
        <section className="flex items-end justify-end">
          <input type="submit" className="btn-primary" value="Submit Damage" />
        </section>
      </form>
    </FormModal>
  );
}
