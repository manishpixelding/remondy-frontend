import { red } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: "#3B7AE0",
    },
    secondary: {
      main: "#FF8748",
    },
    error: {
      main: red.A400,
    },
  },
});

export default theme;
