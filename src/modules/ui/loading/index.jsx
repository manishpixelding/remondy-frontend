import { CircularProgress } from "@mui/material";

export default function Loading({ color = "primary" }) {
  return (
    <section className="flex items-center justify-center w-full h-full">
      <CircularProgress color={color} />
    </section>
  );
}
