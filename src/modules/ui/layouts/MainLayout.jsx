import { ToastContainer } from "react-toastify";

const Navbar = () => {
  return (
    <nav className="grid w-full py-3 shadow-lg bg-red px-10 ">
      <section className="flex w-full justify-between text-white ">
        <div className="flex font-bold uppercase">Remondy System</div>
        <div className="flex">User</div>
      </section>
      <section>
        <article>
          <h1
            className="text-transparent uppercase font-sans font-black "
            style={{
              fontSize: "8em",
              WebkitTextStrokeWidth: "1px",
              WebkitTextStrokeColor: "#000",
            }}>
            Status
          </h1>
        </article>
      </section>
    </nav>
  );
};

export default function MainLayout({ children }) {
  return (
    <div>
      <Navbar />
      <section className="px-10">
        <ToastContainer
          position="top-right"
          autoClose={8000}
          hideProgressBar={false}
          newestOnTop={false}
          draggable={false}
          pauseOnVisibilityChange
          closeOnClick
          pauseOnHover
        />
        {children}
      </section>
    </div>
  );
}
