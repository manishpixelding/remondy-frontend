import Modal from "react-modal";

const FormModal = ({
  children,
  isOpen,
  setIsOpen,
  contentLabel = "FormModal",
}) => {
  const afterOpenModal = () => {
    return 0;
  };

  const closeModal = () => {
    setIsOpen(false);
  };
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };

  return (
    <Modal
      isOpen={isOpen}
      onAfterOpen={afterOpenModal}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel={contentLabel}
      isSearchable={true}>
      <section className="md:w-[60vw] flex-row items-center ">
        {children}
      </section>
    </Modal>
  );
};

export default FormModal;
