import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: [],
};

export const pricingSlice = createSlice({
  name: "pricing",
  initialState,

  reducers: {
    getAllPricing: (state, action) => {
      state.value = action.payload;
    },
    setPricing: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const { setPricing, getAllPricing } = pricingSlice.actions;
export default pricingSlice.reducer;
