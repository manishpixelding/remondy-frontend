import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  showZipcodeForm: false,
  showBrandForm: false,
  showDamageForm: false,
  showPricingForm: false,
  showOrderForm: false,
};

export const orderFormSlice = createSlice({
  name: "orderForm",
  initialState,

  reducers: {
    setShowZipcodeForm: (state, action) => {
      state.showZipcodeForm = action.payload;
    },

    setShowBrandForm: (state, action) => {
      state.showBrandForm = action.payload;
    },

    setShowDamageForm: (state, action) => {
      state.showDamageForm = action.payload;
    },

    setShowOrderForm: (state, action) => {
      state.showOrderForm = action.payload;
    },

    setShowPricingForm: (state, action) => {
      state.showPricingForm = action.payload;
    },
  },
});

export const {
  setShowBrandForm,
  setShowOrderForm,
  setShowDamageForm,
  setShowZipcodeForm,
  setShowPricingForm,
} = orderFormSlice.actions;

export default orderFormSlice.reducer;
