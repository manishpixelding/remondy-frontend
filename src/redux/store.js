import authReducer from "@/src/redux/slices/auth/authSlice";
import userReducer from "@/src/redux/slices/auth/userSlice";
import orderFormReducer from "@/src/redux/slices/order/orderFormSlice";
import orderReducer from "@/src/redux/slices/order/orderSlice";
import { configureStore } from "@reduxjs/toolkit";

const store = configureStore({
  devTools: true,
  reducer: {
    auth: authReducer,
    order: orderReducer,
    orderForm: orderFormReducer,
    user: userReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
