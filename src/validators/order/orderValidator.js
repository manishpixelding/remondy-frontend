import * as Yup from "yup";

export const ZipcodeValidationSchema = Yup.object().shape({
  zipcode: Yup.string(),
});

export const BrandValidationSchema = Yup.object().shape({
  device: Yup.string().required("Brand is required"),
});

export const DamageValidationSchema = Yup.object().shape({
  damage_image: Yup.mixed(),
  damage_desc: Yup.string().required("Damage Description is required"),
});

export const PricingValidationSchema = Yup.object().shape({
  title: Yup.string().required("title is required"),
  base_price: Yup.number().required("base price is required"),
});

export const OrderValidationSchema = Yup.object().shape({
  customer_name: Yup.string().required("Customer is required"),
  address: Yup.string().required("Address is required"),
  shipper: Yup.string().required("Shipper is required"),
  depot: Yup.string().required("Depot is required"),
  ammount: Yup.number().required("Ammount is required"),
});
