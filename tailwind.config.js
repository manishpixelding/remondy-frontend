module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        red: "#FF8748",
        blue: "#3B7AE0",
      },
      fontFamily: {
        sans: ["Roboto", "sans-serif"],
        serif: ["serif"],
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
