import OrderList from "@/src/modules/admin/components/order/OrderList";
import AdminLayout from "@/src/modules/ui/layouts/AdminLayout";
import axios from "axios";
import { useQuery } from "react-query";
export default function Index() {
  const {
    isLoading,
    error,
    data: all_orders,
  } = useQuery("allOrders", () => axios.get("order").then((res) => res.data));

  return (
    <AdminLayout>
      <OrderList all_orders={all_orders} isLoading={isLoading} />
    </AdminLayout>
  );
}
