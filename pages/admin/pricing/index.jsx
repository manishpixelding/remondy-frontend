import PricingList from "@/src/modules/admin/components/pricing/PricingList";
import AdminLayout from "@/src/modules/ui/layouts/AdminLayout";
import axios from "axios";
import { useQuery } from "react-query";

export default function Index() {
  const {
    isLoading,
    error,
    data: pricings,
  } = useQuery("allPricings", () =>
    axios.get("pricing").then((res) => res.data)
  );

  return (
    <AdminLayout>
      <PricingList pricings={pricings} isLoading={isLoading} />
    </AdminLayout>
  );
}
