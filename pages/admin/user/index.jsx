import UserList from "@/src/modules/admin/components/user/UserList";
import AdminLayout from "@/src/modules/ui/layouts/AdminLayout";
import axios from "axios";
import { useQuery } from "react-query";

export default function Index() {
  const {
    isLoading,
    error,
    data: users,
  } = useQuery("allUsers", () =>
    axios.get("account/user/all").then((res) => res.data)
  );

  return (
    <AdminLayout>
      <UserList users={users} isLoading={isLoading} />
    </AdminLayout>
  );
}
