import { setAuth } from "@/src/redux/slices/auth/authSlice";
import { setCurrentUser } from "@/src/redux/slices/auth/userSlice";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export default function Home() {
  const dispatch = useDispatch();
  const router = useRouter();
  const isAuthenticated = useSelector((state) => state.auth.value);

  useEffect(() => {
    (async () => {
      if (!isAuthenticated) {
        const res = await axios.get("account/user", { withCredentials: true });

        if (res.status === 200) {
          dispatch(setAuth(true));
          dispatch(setCurrentUser(res.data));
        } else {
          dispatch(setAuth(false));
          router.push("/auth/login");
        }
      } else {
        router.push("/auth/login");
      }
    })();
  }, []);

  return <div className="bg-slate-200">Welcome to remondy system</div>;
}
