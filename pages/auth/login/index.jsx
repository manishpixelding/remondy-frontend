import MainLayout from "@/src/modules/ui/layouts/MainLayout";
import { setAuth } from "@/src/redux/slices/auth/authSlice";
import toastify from "@/src/utils/toastify";
import { LoginValidationSchema } from "@/src/validators/auth";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";

export default function Index() {
  const router = useRouter();
  const { notify } = toastify();
  const dispatch = useDispatch();
  const formOptions = { resolver: yupResolver(LoginValidationSchema) };
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  const onSubmit = async (data) => {
    const res = await axios.post(`token/`, { ...data });
    if (res && res.status === 200) {
      dispatch(setAuth(true));
      router.push("/dashboard");
    } else {
      dispatch(setAuth(false));
    }
  };

  useEffect(() => {
    (() => {
      Object.values(errors).map((err) => notify("error", err.message));
    })();
  }, [errors, notify]);

  return (
    <MainLayout>
      <section className="flex items-center justify-center w-screen h-screen bg-zinc-50">
        <form
          className="p-8 w-full md:w-[40%] bg-zinc-100 shadow"
          onSubmit={handleSubmit(onSubmit)}
          method="POST">
          <article className="py-2 mb-2 border-b-2 text-zinc-500">
            <h1 className="text-xl font-medium text-zinc-600">
              Remondy System{" "}
            </h1>
            <p className="max-w-md text-xs">
              Welcome to remondy system, please login to proceed with the
              maintaince and co-related works.
            </p>
          </article>
          <div className="flex-col mb-4 ">
            <label htmlFor="email" className="text-sm text-zinc-500">
              Email Address
            </label>
            <input
              type="email"
              name="email"
              {...register("email")}
              className="w-full px-4 py-2 text-sm rounded form-input focus:shadow"
              placeholder="Enter your email address. "></input>
          </div>

          <div className="flex-col mb-4 ">
            <label htmlFor="password" className="text-sm text-zinc-500">
              Enter a password
            </label>
            <input
              type="password"
              name="password"
              {...register("password")}
              className="w-full px-4 py-2 text-sm rounded form-input focus:shadow"
              placeholder="Please enter a password "
            />
          </div>

          <div className="flex-row items-center justify-end mt-5 ">
            <input
              type={"submit"}
              value={"Login Now"}
              className="inline-block px-5 py-2 font-medium text-white uppercase rounded shadow bg-rose-600"
            />
            <h1 className="my-1">
              Do you need an account,{" "}
              <Link href="/auth/register">
                <a className="text-purple-500 ">Register</a>
              </Link>
            </h1>
          </div>
        </form>
      </section>
    </MainLayout>
  );
}
