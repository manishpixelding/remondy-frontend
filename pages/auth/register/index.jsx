import MainLayout from "@/src/modules/ui/layouts/MainLayout";
import toastify from "@/src/utils/toastify";
import { RegisterValidationSchema } from "@/src/validators/auth";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useForm } from "react-hook-form";

export default function Dashboard() {
  const router = useRouter();
  const { notify } = toastify();

  const formOptions = { resolver: yupResolver(RegisterValidationSchema) };
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  const onSubmit = async (data) => {
    try {
      const res = await axios.post("account/register", { ...data });
      notify("success", "User created");
      router.push("/dashboard");
    } catch (e) {
      Object.values(e.response.data).map((errMsg) => notify("error", errMsg));
    }
  };

  useEffect(() => {
    (() => {
      Object.values(errors).map((err) => notify("error", err.message));
    })();
  }, [errors, notify]);

  return (
    <MainLayout>
      <section className="flex items-center justify-center w-screen h-screen bg-zinc-50">
        <form
          className="p-8 w-full md:w-[40%] bg-zinc-100 shadow"
          onSubmit={handleSubmit(onSubmit)}
          method="POST">
          <article className="py-2 mb-2 border-b-2 text-zinc-500">
            <h1 className="text-xl font-medium text-zinc-600">
              Remondy System{" "}
            </h1>
            <p className="max-w-md text-xs">
              Welcome to remondy system, please create an account to proceed
              with the maintaince and co-related works.
            </p>
          </article>
          <div className="flex-col mb-4 ">
            <label htmlFor="email" className="text-sm text-zinc-500">
              Email Address
            </label>
            <input
              type="email"
              name="email"
              {...register("email")}
              className="w-full px-4 py-2 text-sm rounded form-input focus:shadow"
              placeholder="Enter your email address. "></input>
          </div>

          <div className="flex-col mb-4 ">
            <section className="grid grid-cols-2 gap-2">
              <div>
                <label htmlFor="email" className="text-sm text-zinc-500">
                  Firstname
                </label>
                <input
                  type="text"
                  name="first_name"
                  {...register("first_name")}
                  className="w-full px-4 py-2 text-sm rounded form-input focus:shadow"
                  placeholder="Enter your first name. "></input>
              </div>
              <div>
                <label htmlFor="email" className="text-sm text-zinc-500">
                  Lastname
                </label>
                <input
                  type="text"
                  name="last_name"
                  {...register("last_name")}
                  className="w-full px-4 py-2 text-sm rounded form-input focus:shadow"
                  placeholder="Enter your last name. "></input>
              </div>
            </section>
          </div>

          <div className="flex-col mb-4 ">
            <label htmlFor="password" className="text-sm text-zinc-500">
              Enter a password
            </label>
            <input
              type="password"
              name="password"
              {...register("password")}
              className="w-full px-4 py-2 text-sm rounded form-input focus:shadow"
              placeholder="Please enter a password "
            />
          </div>

          <div className="flex-col mb-4 ">
            <label htmlFor="password2" className="text-sm text-zinc-500">
              Confirm your password
            </label>
            <input
              type="password"
              name="password_confirm"
              {...register("password_confirm")}
              className="w-full px-4 py-2 text-sm rounded form-input focus:shadow"
              placeholder="Please confirm your password "
            />
          </div>

          <div className="flex-row items-center justify-end mt-5 ">
            <input
              type={"submit"}
              value={"Register Now"}
              className="w-full px-5 py-2 font-medium text-white uppercase rounded shadow bg-rose-600"
            />
            <h1 className="my-1 text-center">
              Do you already have an account,{" "}
              <Link href="/auth/login">
                <a className="text-purple-500 ">Login</a>
              </Link>
            </h1>
          </div>
        </form>
      </section>
    </MainLayout>
  );
}
